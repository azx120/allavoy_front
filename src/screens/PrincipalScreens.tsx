import React, { Component } from 'react'
import { View} from 'react-native'
import Icon from 'react-native-vector-icons/dist/Feather';
import BottomNavigation, { FullTab } from 'react-native-material-bottom-navigation'
import HomeClientScreen from './systemScreen/HomeClientScreen'
import MisAnuncios from './systemScreen/MisAnuncios'
import SettingScreen from './systemScreen/SettingScreen'
import NotificationScreen from './systemScreen/NotificationScreen'


export default class PrincipalScreens extends Component {
  tabs = [
    {
      key: 'HomeClientScreen',
      icon: 'search',
      label: '',
      screen: <HomeClientScreen navigation={this.props.children} />,
      barColor: '#ffff',
      pressColor: 'rgba(255, 255, 255, 0.16)'
    },
    {
      key: 'MisAnuncios',
      icon: 'folder',
      label: '',
      screen: <MisAnuncios navigation={this.props.children} />,
      barColor: '#ffff',
      pressColor: 'rgba(255, 255, 255, 0.16)'
    },
    {
      key: 'NotificationScreen',
      icon: 'bell',
      label: '',
      screen: <NotificationScreen navigation={this.props.children} />,
      barColor: '#ffff',
      pressColor: 'rgba(255, 255, 255, 0.16)'
    },
    {
      key: 'SettingScreen',
      icon: 'settings',
      label: '',
      screen: <SettingScreen navigation={this.props.children} />,
      barColor: '#ffff',
      pressColor: 'rgba(255, 255, 255, 0.16)'
    }
  ]

  state = {
    activeTab: 'HomeClientScreen'
  }

  renderScreen = () => (
    this.state.activeTab == 'HomeClientScreen' && this.tabs[0].screen ||
    this.state.activeTab == 'MisAnuncios' && this.tabs[1].screen ||
    this.state.activeTab == 'NotificationScreen' && this.tabs[2].screen ||
    this.tabs[3].screen

  )

  renderIcon = icon => ({ isActive }) => (
    <Icon size={25} color="#929392" name={icon} style={{ marginTop: 10 }} />
  )

  renderTab = ({ tab, isActive }) => (
    <FullTab
      isActive={isActive}
      key={tab.key}
      label={tab.label}
      renderIcon={this.renderIcon(tab.icon)}
      style={{backgroundColor:"black"}}
    />
  )

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 1, }}>
          {this.renderScreen()}
        </View>
        <BottomNavigation
          activeTab={this.state.activeTab}
          onTabPress={newTab => this.setState({ activeTab: newTab.key })}
          renderTab={this.renderTab}
          tabs={this.tabs}
        />
      </View>
    )
  }
}