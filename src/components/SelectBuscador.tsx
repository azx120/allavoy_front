import React, { useState } from "react";
import { View, StyleSheet, Text } from "react-native";
import { color } from "../styles/colors";
import RNPickerSelect from 'react-native-picker-select';

function SelectBuscador(props){
    const array = [];
    const useData = props.usedata;
    props.dataForm[useData].map(n => {
        let arrayTemnp = { label: n.nombre, value: n.id };
        array.push(arrayTemnp);
        //arrayTemnp = { label: n.nombre, value: n.id };
    })
    const [ onValueChange, setOnValueChange ] = useState("");
    return(


        
		<View style={styles.container}>
            <RNPickerSelect	
                placeholder={{
                    label: props.usedata,
                    value: null,
                    color: color.SECONDARYCOLOR,
                }}
                value={props.value}
                onValueChange={props.onValueChange}
                items={array}

            />
        </View>



    )


}


const styles = StyleSheet.create({
	container:{
		color:color.PRIMARYCOLOR,
		borderColor:color.PRIMARYCOLOR,
		borderWidth: 1,
		borderTopLeftRadius: 4,
		borderTopRightRadius: 4,
		borderBottomLeftRadius: 4,
		borderBottomRightRadius: 4,
	},
});

export default SelectBuscador