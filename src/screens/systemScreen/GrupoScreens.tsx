import React, { useContext, useState } from 'react'
import { Text, View, TouchableOpacity, StatusBar, Alert, ScrollView } from 'react-native'
import ToolBar from '../../components/Toolbar'
import { AuthContex } from '../../context/UsuarioContext'
import ElementListVecino from '../../components/ElementListVecino'
import {color} from '../../styles/colors'
import { useNavigation } from '@react-navigation/native'



export default function GrupoScreens(props) {
    const navigator = useNavigation()
    const states = [
        {
            id: 1,
            nombre: "grupo 1",
            integrantes: [{
                id: 1,
                name: 'Rosanyelis',
                lastname: 'Mendoza',
                phone: '+58 414-8035352',
                address: 'Avenida 4, charallave, Caracas',
                familares: [{
                    id: 1,
                    nombre: "Maria Gonzales",
                    numero: "+58 412-6035324"
                }, {
                    id: 2,
                    nombre: "Roberto Mendonza",
                    numero: "+58 416-4565352"
                }]
            },
            {
                id: 2,
                name: 'Alfredo',
                lastname: 'Zerpa',
                phone: '+58 416-9405794',
                address: 'Ciudad Alianza, Guacara, Carabobo',
                familares: [{
                    id: 1,
                    nombre: "Alfredo Zerpa",
                    numero: "+58 0416-3408685"
                }, {
                    id: 2,
                    nombre: "Graciela Perez",
                    numero: "+58 0412-4119019"
                }]
            }]
        },
        {
            id: 2,
            nombre: "grupo 2",
            integrantes: [{
                id: 3,
                name: 'Rafael',
                lastname: 'Salaz',
                phone: '+58 416-9405794',
                address: 'Avenida 4, charallave, Caracas',
                familares: [{
                    id: 1,
                    nombre: "Yenifer Salaz",
                    numero: "+58 416-9405794"
                }, {
                    id: 2,
                    nombre: "Carlos Hernandez",
                    numero: "+58 416-9405794"
                }]
            },
            {
                id: 4,
                name: 'Jose',
                lastname: 'Rivas',
                phone: '+58 416-9405794',
                address: 'Ciudad Alianza, Guacara, Carabobo',
                familares: [{
                    id: 1,
                    nombre: "Rodrigo Rivas",
                    numero: "+58 416-9405794"
                }, {
                    id: 2,
                    nombre: "Carolos Rivas",
                    numero: "+58 416-9405794"
                }]
            },
            {
                id: 2,
                name: 'Alfredo',
                lastname: 'Zerpa',
                phone: '+58 416-9405794',
                address: 'Avenida 4, charallave, Caracas',
                familares: [{
                    id: 1,
                    nombre: "Alfredo Zerpa",
                    numero: "+58 0416-3408685"
                }, {
                    id: 2,
                    nombre: "Graciela Perez",
                    numero: "+58 0412-4119019"
                }]
            }]
        },
        {
            id: 3,
            nombre: "grupo santa rosa",
            integrantes: [
                {
                    id: 8,
                    name: 'Jenifer',
                    lastname: 'Rodriguez',
                    phone: '+58 416-9405794',
                    address: 'av. Bolivar, Valencia, Carabobo',
                    familares: [{
                        id: 1,
                        nombre: "Ricardo Romero",
                        numero: "+58 416-9405794"
                    }, {
                        id: 2,
                        nombre: "Juan Rodriguez",
                        numero: "+58 416-9405794"
                    }]
                }, {
                    id: 3,
                    name: 'Cristofer',
                    lastname: 'Zapata',
                    phone: '+58 416-9405794',
                    address: 'Avenida 4, chacao, Caracas',
                    familares: [{
                        id: 1,
                        nombre: "Cristofer",
                        numero: "+58 416-9405794"
                    }, {
                        id: 2,
                        nombre: "Maria",
                        numero: "+58 416-9469794"
                    }]
                },
                {
                    id: 4,
                    name: 'mario',
                    lastname: '',
                    phone: '+58 416-9805794',
                    address: 'Ciudad Alianza, Guacara, Carabobo',
                    familares: [{
                        id: 1,
                        nombre: "",
                        numero: "+58 416-0505794"
                    }, {
                        id: 2,
                        nombre: "",
                        numero: "+58 416-9405794"
                    }]
                },
                {
                    id: 2,
                    name: 'mercelo',
                    lastname: '',
                    phone: '+58 414-8505794',
                    address: 'Ciudad Alianza, Guacara, Carabobo',
                    familares: [{
                        id: 1,
                        nombre: "",
                        numero: "+58 416-9405794"
                    }, {
                        id: 2,
                        nombre: "",
                        numero: "+58 412-9462794"
                    }]
                }]
        }

    ]
    //const [login, loginAction] = useContext(UserContext)
    return (
        <ScrollView style={{ flex: 1 }}>
            <StatusBar
                backgroundColor={color.WHITE}
                barStyle='dark-content'
                translucent={true}
            />
             <ToolBar titulo='Vecino'
                    onPressLeft={() => goToBackScreen()}
                    iconLeft={require('../../sources/img/back.png')} 
                    />

            <View >
                <ElementListVecino
                navigation={props.navigation}
                grupos={states}
            /> 
            </View>
           


        </ScrollView>

    )
    function goToBackScreen() {
        navigator.goBack()
    }

}