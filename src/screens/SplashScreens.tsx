import React, { useContext, useEffect } from 'react'
import { View, StatusBar } from 'react-native'
import * as Animateable from 'react-native-animatable'
import { splashStyles } from '../styles/styles'
import { getUsuario } from '../storage/UsuarioAsyncStorage'
import { getAnuncios } from '../storage/AnunciosAsyncStorage'
import { getDataForm } from '../storage/FormDataAsyncStorage'
import { AuthContex } from '../context/UsuarioContext'
import { useNavigation } from '@react-navigation/native'



const SplashScreen = () => {

    const navigator = useNavigation()
    
    const { sing } = useContext(AuthContex)

    useEffect(() => {
        fetchSession(sing)
    }, [])
    const goToScreen = (routeName) => {
        navigator.navigate(routeName)
    } 
    return (
        <View style={splashStyles.image}>
            <StatusBar translucent backgroundColor='rgba(0,0,0,0.2)' />
            <Animateable.Image
                animation="pulse"
                easing="ease-out"
                iterationCount="infinite"
                style={{
                    width: 200,
                    height: 113,
                    margin: 100
                }}
                source={require('../sources/img/favicon.gif')}
            />
        </View>
    )
    async function fetchSession(sing) {
        const responseUser = await getUsuario();
        let responseAnuncios = await getAnuncios();
        let responseDataform = await getDataForm();

        if (responseAnuncios == null) {
            responseAnuncios = [];
        }
        if (responseDataform == null) {
            responseDataform = [];
        }

        if (responseUser == null) {
            setTimeout(() => {
                goToScreen('SliderHomeScreen')
            }, 3000)
            return
        }
        sing(responseUser, responseAnuncios, responseDataform)
        setTimeout(() => {
            goToScreen('PrincipalScreen')
        }, 500)
    }

}

export default SplashScreen;