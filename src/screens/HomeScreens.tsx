import React, { useState, useEffect} from 'react'
import { Text, View, I18nManager, Image, StyleSheet, Platform} from 'react-native'
import MyButtonHome from '../components/ButtonHome'
import AppIntroSlider from 'react-native-app-intro-slider';
import { useNavigation } from '@react-navigation/native';
import { color } from '../styles/colors';
import  BotonNumero  from '../components/BotonNumero'
import { TouchableOpacity } from 'react-native-gesture-handler';


export const slides = [
    {
        key: '1',
        title: 'Conexcion con tus conocidos',
        text: 'Description.\nSay something cool',
        image: require('../sources/img/slide1.jpg'),
        backgroundColor: require('../sources/img/slide1.jpg'),
    },
    {
        key: '2',
        title: 'Disponible para todo el mundo',
        text: 'Other cool stuff',
        image: require('../sources/img/slide2.jpg'),
        backgroundColor: '#febe29',
    },
    {
        key: '3',
        title: 'Seguridad a tu persona',
        text: 'I\'m already out of descriptions\n\nLorem ipsum bla bla bla',
        image: require('../sources/img/slide3.jpg'),
        backgroundColor: '#22bcb5',
    }
];


const isAndroidRTL = I18nManager.isRTL && Platform.OS === 'android';

export  const SliderHomeScreen = () => {
    const navigator = useNavigation()


    useEffect(() => {
   
    }, [])


    const [state, setState] = useState({
        showRealApp: false
    })
    


    const _renderItem = ({ item }) => {
        return (

            <View >
                {/* <Text style={{color: "white", zIndex: 1, position: 'absolute', fontSize: 25 }}>{item.title}</Text> */}
                <Image style={{ width: "100%", height: "100%" }} source={item.image} />

            </View>
        );
    }

    const _renderButton = () => {
        return (

            <Text style={{ color: "white", fontSize: 25 }}>hola</Text>

        );
    }
    const _onDone = () => {

        setState({ showRealApp: true });
    }

    const goToScreen = (routeName) => {
        navigator.navigate(routeName)
    }
    if (state.showRealApp) {
        return <SliderHomeScreen />;
    } else {
        return (
            <>
                <View style={{ flex:1,justifyContent:"space-between", paddingTop:45, paddingBottom:25,}}>
                    <View style={{
                        alignItems:"center"
                    }}>
                        <Text style={{
                            fontSize:25,
                            marginTop:10,
                            textAlign:"center",
                            color:color.SECONDARYCOLOR,
                            fontFamily:"roboto-allavoy"
                        }}>
                            Bienvenido
                        </Text>
                    </View>
                    
                    <View style={{
                        height:350,
                        paddingLeft:"10%",
                        paddingRight:"10%",    
                    }}>
                        <AppIntroSlider renderItem={_renderItem} data={slides} onDone={_onDone} showDoneButton={false} showNextButton={false} />
                    </View>
                    
                    <View style={{
                        justifyContent:"center",
                        alignItems:"center"
                    }}>
                        <TouchableOpacity  onPress={() => { goToScreen('IngresarNumeroScreen') }}>
                        <BotonNumero 
                            TituloNumero='Aceptar y Continuar'
                           
                        />
                        </TouchableOpacity>
                        
                        
                        <Text style={{
                            textAlign:"center",
                            marginTop:10,
                            fontSize:12
                        }}>
                            Lee nuestra <Text
                                onPress={() => { goToScreen('LoginScreen') }} 
                                style={{color:color.SECONDARYCOLOR}}
                            >
                                Politica de Privacidad
                            </Text>
                        </Text>
                        <Text style={{
                            textAlign:"center",
                            fontSize:12,
                        }}>
                            Al pulsar "Acepto" aceptas nuestras
                        </Text>
                        <Text
                            onPress={() => { goToScreen('RegisterScreen') }} 
                            style={{
                                color:color.SECONDARYCOLOR,
                                textAlign:"center",
                                fontSize:12
                            }}
                        >
                            Politica de Privacidad
                        </Text>
                    </View>
                </View>

            </>
        );
    }
 

}




const styles = StyleSheet.create({
    flexOne: {
        flex: 1,
    },
    flatList: {
        flex: 1,
        flexDirection: isAndroidRTL ? 'row-reverse' : 'row',
    },
    paginationContainer: {
        position: 'absolute',
        bottom: 16,
        left: 16,
        right: 16,
        justifyContent: 'center',
    },
    paginationDots: {
        height: 16,
        margin: 16,
        flexDirection: isAndroidRTL ? 'row-reverse' : 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    dot: {
        width: 10,
        height: 10,
        borderRadius: 5,
        marginHorizontal: 4,
    },
    leftButtonContainer: {
        position: 'absolute',
        left: 0,
    },
    rightButtonContainer: {
        position: 'absolute',
        right: 0,
    },
    bottomButton: {
        flex: 1,
        backgroundColor: 'rgba(0, 0, 0, .3)',
        alignItems: 'center',
        justifyContent: 'center',
    },
    transparentBottomButton: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonText: {
        color: 'white',
        fontSize: 20,
        padding: 12,
    },
});





