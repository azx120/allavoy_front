import React, { useEffect, useState } from 'react'
import Icon from 'react-native-vector-icons/dist/Feather';
import { Text, TouchableOpacity, StyleSheet } from 'react-native'



function AgregarProducto(){

    return(
        <TouchableOpacity>
                
            <Icon style={styles.IconAgregar} name='eye' size={25} color="#fff"/>

        </TouchableOpacity>
    )

}


const styles = StyleSheet.create({
    IconAgregar:{
        textAlign:"center"
    }
})


export default AgregarProducto