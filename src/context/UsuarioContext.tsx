import React, { createContext, useReducer } from 'react'
import apiApp from '../api/api'
import { userReducer, Authstate } from './UserReducer';
import { confirmNumberResponse, User, RegisterResponse } from '../interfaces/UserInterface';
import { Anuncio, registerAnuncio, editAnuncio } from '../interfaces/AnuncioInterface';
import { useNavigation } from '@react-navigation/native';
import { DataTable } from 'react-native-paper';
import { DataForm } from '../interfaces/DataFormInterface';

type AuthContextProps = {
    errorMessage: string;
    access_token: string | null;
    user: User | null | '';
    anuncios: Anuncio[] | null | '';
    dataForm: DataForm[] | null | '';
    status: 'cheking' | 'authenticated' | 'not-authenticated' | 'registered-phone' | 'registered-dates';
    singUp: (data: any) => void;
    sing: (data: any, anuncios: any, dataForm: any) => void;
    logOut: () => void;
    sendCode: (phone: string) => void;
    confirmCode: (data: any) => void;
    createAnuncio: (data: any) => void;
    editAnuncio: (data: any, anuncios: any) => void;
    removeError: () => void;
    numberTemporal: string;
}

const initialSatate: Authstate = {
    status: 'cheking',
    access_token: null,
    user: null,
    errorMessage: null,
    anuncios: null ,
    numberTemporal: null,
    dataForm: null
    
}

const AuthContex = createContext({} as AuthContextProps);


const UserProvider = ({ children }: any) => {

    const [login, dispatch] = useReducer(userReducer, initialSatate);

    const sing = (user, anuncios, dataForm) => {

        dispatch({ type: 'sing-in', payload: { anuncios: anuncios, user: user, dataForm: dataForm} })
    }

    const singUp = async (data) => {
        
        try {
           
            const resp = await apiApp.post<RegisterResponse>('/registro', data);

            dispatch({ type: 'sing-up', payload: { user: resp.data.user, anuncios: resp.data.anuncios, dataForm: resp.data.dataForm } })
        } catch (error) {

            dispatch({ type: 'addError', payload: error.response.data.message })
        }
    }

    const sendCode =  async (phone) => {
        try {
            const resp = await apiApp.post('/registroNumber', {'phone':phone});
            console.log(resp.data.status);
            dispatch({ type: 'numberTemporal', payload:  phone  })

          
        } catch (error) {

            dispatch({ type: 'addError', payload: error.response.data.message })
    
        }
    }

    const logOut = () => {
        dispatch({ type: 'sing-out', payload: { access_token: null, user: null } })

    }

    const confirmCode = async (data) => {
        try {
            
            const resp = await apiApp.post<confirmNumberResponse>('/confirmCode', data)
            console.log(data)
            console.log(resp.data.user)
            dispatch({ type: 'confirmedNumber', payload: { access_token: resp.data.access_token, user: resp.data.user, anuncios: resp.data.anuncios, dataForm: resp.data.dataForm } })
        } catch (error) {

            dispatch({ type: 'addError', payload: error.response.data.message })
        }
    }

    const createAnuncio = async (data) => {
        try {
            const resp = await apiApp.post<registerAnuncio>('/createAnuncio', data)

            dispatch({ type: 'createAnuncio', payload: { anuncios: resp.data.data } })
        } catch (error) {

            dispatch({ type: 'addErrorsistem', payload: error.response.data.message })
        }
    }

    const editAnuncio = async (data, anuncios) => {

        try {
            const resp = await apiApp.post<editAnuncio>('/editAnuncio', data)

           dispatch({ type: 'editAnuncio', payload: { anuncio: resp.data.data, anuncios: anuncios } })
        } catch (error) {

            dispatch({ type: 'addErrorsistem', payload: error.response.data.message })
        }
    }



const removeError = () => { }

return (
    <AuthContex.Provider value={{
        ...login,
        sing,
        singUp,
        logOut,
        sendCode,
        confirmCode,
        createAnuncio,
        removeError,
        editAnuncio
    }} >
        {children}
    </AuthContex.Provider>
)
}


export { AuthContex, UserProvider }