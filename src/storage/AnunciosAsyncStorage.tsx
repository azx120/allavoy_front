import AsyncStorage from '@react-native-community/async-storage'

const ANUNCIOS_KEY = '@anuncios:key'


async function saveAnuncios(anuncios){
    try {
        await AsyncStorage.setItem(ANUNCIOS_KEY, JSON.stringify(anuncios))
        return JSON.stringify(anuncios)
    } catch (error) {
        //Error
        console.log('error al guardar: ' + error.message)
        return 'Error de sintaxis'
    }
}

async function getAnuncios(){
    try {
        const item = await AsyncStorage.getItem(ANUNCIOS_KEY)
        return JSON.parse(item)
    } catch (error) {
        // Error retrieving data
        console.log("Error al recuperar:" + error.message)
        return null
    }
}

async function deleteAnuncios(){
    try {
        await AsyncStorage.removeItem(ANUNCIOS_KEY)
        const item = await AsyncStorage.getItem(ANUNCIOS_KEY)
        return (item == null?"anuncio removido":"anuncio no removido")
    } catch (error) {
        console.log("Error al eliminar" + error.message)
        return "Error de sintaxis"
    }
}

export {saveAnuncios, getAnuncios, deleteAnuncios  }