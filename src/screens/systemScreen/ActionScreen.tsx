import React, { useContext, useEffect } from 'react'
import { Text, View, TouchableOpacity, StatusBar, Alert, Image } from 'react-native'
import Icon from 'react-native-vector-icons/dist/Feather';
import { DataTable } from 'react-native-paper';
import { color } from '../../styles/colors'
import { useNavigation } from '@react-navigation/native';



export default function SettingScreen(props) {
    const navigator = useNavigation()
    return (
        <View style={{ flex: 1, alignItems: 'center', marginTop: 80 }}>
            <StatusBar
                backgroundColor={color.WHITE}
                barStyle='dark-content'
                translucent={true}
            />

            <DataTable>

                <TouchableOpacity onPress={() => goToScreen("AnunciosScreen")} >

                    <DataTable.Row style={{ height: 70, padding: 10 }}>
                        <DataTable.Cell style={{ flex: 0.3 }}>
                            <View>
                                <Icon size={30} color="grey" name={"plus-square"} />
                            </View>

                        </DataTable.Cell>
                        <DataTable.Cell style={{ flex: 2 }}>
                            <Text style={{ fontSize: 20 }} > Crear Anuncio </Text>
                        </DataTable.Cell>

                        <DataTable.Cell style={{ flex: 0.3 }}>
                            <View>
                                <Image source={require('../../sources/img/next.png')} style={{ height: 15, width: 15 }} />
                            </View>
                        </DataTable.Cell>

                    </DataTable.Row>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => goToScreen("GrupoScreens")}>

                    <DataTable.Row style={{ height: 70, padding: 10 }} >
                        <DataTable.Cell style={{ flex: 0.3 }}>
                            <View>
                                <Icon size={30} color="grey" name={"users"} />
                            </View>

                        </DataTable.Cell>
                        <DataTable.Cell style={{ flex: 2 }}>
                            <Text style={{ fontSize: 20 }} > Mis Grupos </Text>
                        </DataTable.Cell>

                        <DataTable.Cell style={{ flex: 0.3 }}>
                            <View>
                                <Image source={require('../../sources/img/next.png')} style={{ height: 15, width: 15 }} />
                            </View>
                        </DataTable.Cell>

                    </DataTable.Row>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => goToScreen("GrupoScreens")} >

                    <DataTable.Row style={{ height: 70, padding: 10 }}>
                        <DataTable.Cell style={{ flex: 0.3 }}>
                            <View>
                                <Icon size={30} color="grey" name={"user"} />
                            </View>

                        </DataTable.Cell>
                        <DataTable.Cell style={{ flex: 2 }}>
                            <Text style={{ fontSize: 20 }}> Inivitado en Grupos </Text>
                        </DataTable.Cell>

                        <DataTable.Cell style={{ flex: 0.3 }}>
                            <View>
                                <Image source={require('../../sources/img/next.png')} style={{ height: 15, width: 15 }} />
                            </View>
                        </DataTable.Cell>

                    </DataTable.Row>
                </TouchableOpacity>

                <TouchableOpacity   >

                    <DataTable.Row style={{ height: 70, padding: 10 }}>
                        <DataTable.Cell style={{ flex: 0.3 }}>
                            <View>
                                <Icon size={30} color="grey" name={"alert-circle"} />
                            </View>

                        </DataTable.Cell>
                        <DataTable.Cell style={{ flex: 2 }}>
                            <Text style={{ fontSize: 20 }} > Mis alertas </Text>
                        </DataTable.Cell>

                        <DataTable.Cell style={{ flex: 0.3 }}>
                            <View>
                                <Image source={require('../../sources/img/next.png')} style={{ height: 15, width: 15 }} />
                            </View>
                        </DataTable.Cell>

                    </DataTable.Row>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => goToScreen("FavoriteScreen")} >

                    <DataTable.Row style={{ height: 70, padding: 10 }}>
                        <DataTable.Cell style={{ flex: 0.3 }}>
                            <View>
                                <Icon size={30} color="grey" name={"star"} />
                            </View>

                        </DataTable.Cell>
                        <DataTable.Cell style={{ flex: 2 }}>
                            <Text style={{ fontSize: 20 }} > Favoritos </Text>
                        </DataTable.Cell>

                        <DataTable.Cell style={{ flex: 0.3 }}>
                            <View>
                                <Image source={require('../../sources/img/next.png')} style={{ height: 15, width: 15 }} />
                            </View>
                        </DataTable.Cell>

                    </DataTable.Row>
                </TouchableOpacity>



            </DataTable>
        </View>

    )

    function goToScreen(routeName) {
        navigator.navigate(routeName);
    }


}