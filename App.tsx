import React from 'react';
import {Navigation} from './src/navigation/AppNavigation'
import { UserProvider } from "./src/context/UsuarioContext";
import { NavigationContainer } from '@react-navigation/native';


export const AppState =({children}: {children: JSX.Element | JSX.Element[]}) => {
  return (
    <UserProvider>
      {children}
    </UserProvider>
    
  ) 
}

const App = () => {
  return (
    <NavigationContainer>
      <AppState>
        <Navigation /> 
      </AppState> 
    </NavigationContainer>
    
  ) 
}
export default App;