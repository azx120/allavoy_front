import React, { useContext, useEffect, useState } from 'react'
import { Text, View, TouchableOpacity, StatusBar, Alert, BackHandler, ScrollView, Image, Modal, Button } from 'react-native'
import Icon from 'react-native-vector-icons/dist/Feather';
import {color} from '../styles/colors'

function CajaNotificaciones(){

    const [showModal, setshowModal] = useState(false);

    

    return(
        

        <View  style={{
            padding:10, 
            flexDirection: "row",
            flexWrap: "wrap"
        }}>
            <Modal
                animationType={'fade'}
                transparent={true}
                visible={showModal}
                onRequestClose={ () => {
                
                } }
                
            >
                <View style={{
                    flex:1,
                    backgroundColor:"rgba(1,1,1,0.5)",
                    height:"100%",
                    width:"100%",
                    justifyContent:"center",
                    alignItems:"center"
                }}>
                    <View style={{
                        backgroundColor:color.WHITE,
                        height:250,
                        width:250
                    }}>
                        <Image
                            source={ require('../sources/img/ads.jpg') }
                            style={{ width:250, height:200,}}
                        />
                        <View style={{
                            flexDirection: "row",
                            flexWrap: "wrap"
                        }}>
                            <View style={{width:"33.3%",height:50, justifyContent:"center", alignItems:"center"}}>
                                <Icon
                                    name='mail'
                                    color='#d99230'
                                    size={25}
                                />
                            </View>
                            <View style={{width:"33.3%",height:50, justifyContent:"center", alignItems:"center"}}>
                                <Icon
                                    name='phone-call'
                                    color='#d99230'
                                    size={25}
                                />
                            </View>
                            <View style={{width:"33.3%",height:50, justifyContent:"center", alignItems:"center"}}>
                                <Icon
                                    name='info'
                                    color='#d99230'
                                    size={25}
                                />
                            </View>
                            <Text
                            onPress={() => {
                                setshowModal(!showModal);
                            }}
                            style={{
                                color:color.SECONDARYCOLOR,
                                fontSize:30,
                                lineHeight:30,
                                paddingLeft:7,
                                paddingRight:7,
                                fontWeight:"bold",
                                position:"absolute",
                                top:-200,
                                right:0,
                                backgroundColor:"rgba(1,1,1,0.5)",
                            }}
                        >
                            x
                        </Text>
                        </View>
                    </View>
                </View>

            </Modal>

            <View style={{
                width:"15%",
            }}>
                <TouchableOpacity
                    onPress={() => {
                        setshowModal(!showModal);
                    }}
                >
                    <Image
                        source={ require('../sources/img/ads.jpg') }
                        style={{ width:50, height:50, borderRadius:50}}
                    />
                </TouchableOpacity>
            </View>
            <View style={{
                width:"65%",
            }}> 
                <View style={{
                    flexDirection: "row",
                    flexWrap: "wrap"
                }}>
                    <Text style={{ 
                        color:color.PRIMARYCOLOR,
                        fontSize:18,
                        fontWeight:"bold",
                    }}>
                        Gey=
                    </Text>
                    <Text style={{
                        color:color.PRIMARYCOLOR,
                        fontSize:18,
                        fontWeight:"bold",
                    }}>
                        "Comercial Ayala"
                    </Text>
                </View>
                <Text style={{
                    color:color.PRIMARYCOLOR,
                    fontSize:18,
                }}>
                    tenemos el respuesto original
                </Text>

            </View>
            <View style={{
                width:"20%",
            }}>
                <Text style={{
                    color:color.PRIMARYCOLOR,
                    fontSize:16,
                    fontWeight:"bold",
                    textAlign:"right"
                }}>
                    Hoy, 13:15
                </Text>
                <Text style={{
                    color:color.PRIMARYCOLOR,
                    fontSize:17,
                    fontWeight:"bold",
                    textAlign:"right"
                }}>
                    86/54
                </Text>
            </View>
        </View>

    )


}

export default CajaNotificaciones
