import React, { useState } from 'react'
import {
    Text,
    View,
    TouchableOpacity,
    StatusBar,
    ScrollView
} from 'react-native'
import { mainStyles, registroStyles } from '../../styles/styles'
import ToolBar from '../../components/Toolbar'
import {color} from '../../styles/colors'
import { CheckBox, SocialIcon, Button } from 'react-native-elements'
import { useNavigation } from '@react-navigation/native'


export default function IdiomaSettingScreen(props) {
    const navigator = useNavigation()
    const [hidePassword, setHidePassword] = useState(false)

    return (
        <View style={{ flex: 1}}>
            <ScrollView
                keyboardDismissMode='on-drag'
                keyboardShouldPersistTaps='always'
                style={{ backgroundColor: color.WHITE }}>
                <StatusBar backgroundColor={color.WHITE} translucent={true} />
                <ToolBar titulo='Ayuda'
                    onPressLeft={() => navigator.goBack()}
                    iconLeft={require('../../sources/img/back.png')} 
                    />
                <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', marginTop:50 }}>
                    <Text style={{ textAlign: 'center', fontFamily: 'summernote' }}>AQUI ESTARA LA CONFIGURACION DE IDIOM</Text>

                
                    <Button title="Cancelar" onPress={() => navigator.goBack()} type="clear" />
                </View>
                   
              
            </ScrollView>
        </View>
    )

}