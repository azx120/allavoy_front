import React, { useContext, useEffect, useState } from "react"
import { View, Text, Image, StyleSheet, Modal } from "react-native"
import { color } from "../../styles/colors";



function Chat(){

    const [showModal, setshowModal] = useState(false);

    return(


        <View style={{
            flex:1,
            alignItems:"center",
            justifyContent:"center",
            padding:20
        }}>
            <Modal
                animationType={'fade'}
                transparent={true}
                visible={showModal}
                onRequestClose={ () => {
                
                } }
                
            >
                <View style={{
                    flex:1,
                    backgroundColor:"rgba(1,1,1,0.5)",
                    height:"100%",
                    width:"100%",
                    justifyContent:"center",
                    alignItems:"center"
                }}>
                    <View style={{
                        backgroundColor:color.WHITE,
                        height:250,
                        width:300,
                        padding:15
                    }}>
                        <Text
                            onPress={() => {
                                setshowModal(!showModal);
                            }}
                            style={{
                                color:color.SECONDARYCOLOR,
                                fontSize:30,
                                lineHeight:30,
                                fontWeight:"bold",
                                textAlign:"right",
                                paddingRight:5
                            }}
                        >
                            x
                        </Text>
                        <Text style={{
                            textAlign:"center",
                            fontWeight:"bold",
                            fontSize:20,
                            color:color.PRIMARYCOLOR
                        }}>
                            Comercial "Ayala"
                        </Text>
                        <Text style={{
                            fontWeight:"bold",
                            fontSize:18,
                            marginTop:10
                        }}>
                            Ver Infomacion
                        </Text>
                        <Text style={{
                            fontWeight:"bold",
                            fontSize:18,
                            marginTop:10
                        }}>
                            Llamar por Teléfono
                        </Text>
                        <Text style={{
                            fontWeight:"bold",
                            fontSize:18,
                            marginTop:10
                        }}>
                            Enviar mensaje opr whatsapp
                        </Text>
                        <Text style={{
                            fontWeight:"bold",
                            fontSize:18,
                            marginTop:10
                        }}>
                            Ver lista de Notificaciones
                        </Text>
                    </View>
                </View>

            </Modal>

            <View style={{
                width:"100%",
                borderWidth:3,
                borderColor:color.PRIMARYCOLOR,
                borderTopLeftRadius: 20,
                borderBottomLeftRadius: 20,
                borderBottomRightRadius: 20,
                flexDirection: "row",
                flexWrap: "wrap",
                padding:10
            }}>
                <View style={{
                    width:"65%",

                }}>
                    <Text style={styles.Titulos}>
                        Guayas - Guayaquil
                    </Text>
                    <Text style={styles.Titulos}>
                        Camioneta
                    </Text>
                    <Text style={styles.Titulos}>
                        Chevrolet
                    </Text>
                    <Text style={styles.Titulos}>
                        Dimax 2021
                    </Text>
                    <Text style={styles.Titulos}>
                        Carburador a dieles
                    </Text>
                </View>
                <View style={{
                    width:"35%"
                }}>
                    <Image
                        source={ require('../../sources/img/ads.jpg') }
                        style={{ width: "100%", height:100}}
                    />
                </View>

                <View style={{width:"100%"}}>
                    <Text>
                        Necesito Comprar un nuevo repuesto ¿alguien tiene el respuesto original?
                    </Text>
                </View>
                <View style={{width:"100%"}}>
                    <Text style={styles.Horas}>
                        15:30
                    </Text>
                </View>

            </View>
            <View style={{
                marginTop:30,
                width:"100%",
                borderWidth:3,
                borderColor:color.PRIMARYCOLOR,
                borderTopRightRadius: 20,
                borderBottomLeftRadius: 20,
                borderBottomRightRadius: 20,
                padding:10
            }}>
                <View>
                    <Text style={styles.Titulos}
                        onPress={() => {
                            setshowModal(!showModal);
                        }}
                    >
                        Comercial "Ayala"
                    </Text>
                </View>


                <View>
                    <Text>
                        si ya que puedo estar trranquilo
                    </Text>
                    <Text>
                        si ya que puedo estar trranquilo
                    </Text>
                    <Text>
                        si ya que puedo estar trranquilo
                    </Text>
                    <Text>
                        si ya que puedo estar trranquilo
                    </Text>
                </View>
                <View>
                    <Text style={styles.Horas}>
                        15:30
                    </Text>
                </View>

            </View>
        </View>

    )

}


const styles = StyleSheet.create({
    Titulos: {
      fontWeight:"bold",
      fontSize:15,
      color:color.PRIMARYCOLOR
    },
    Horas:{
        color:color.PRIMARYCOLOR, 
        textAlign:"right", 
        fontWeight:"bold"
    }
  
  });




export default Chat