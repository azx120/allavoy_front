import React from 'react'
import { Text, View, StyleSheet } from 'react-native'
import { color } from '../styles/colors'

function ButtonSolicitud(){

    return(
        <View style={styles.MyButtonSolicitud}>
            <Text style={styles.MytextSolicitud}> Enviar solicitud </Text> 
        </View>
    )

}



const styles = StyleSheet.create({

    MyButtonSolicitud:{
        width: 250,
        backgroundColor: color.SECONDARYCOLOR,
        marginTop:20,
        padding:2,
        borderBottomLeftRadius: 35,
        borderBottomRightRadius: 35,
        borderTopLeftRadius: 35,
        borderTopRightRadius: 35,
    },
    MytextSolicitud:{
        textAlign: 'center',
        fontSize: 17,
        color: color.PRIMARYCOLOR,
        paddingVertical: 10,
        fontFamily: 'Poppins-Bold',
        fontWeight:"bold"
    }

})
export default ButtonSolicitud