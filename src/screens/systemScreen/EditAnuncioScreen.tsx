import React, { useContext, useState } from 'react'
import { View, StyleSheet, Text, StatusBar, ScrollView, TouchableOpacity, TextInput } from 'react-native'
import { Formik } from 'formik';
import { mainStyles, registroStyles } from '../../styles/styles'
import MyButtonHome from '../../components/ButtonHome'
import MyTextInput from '../../components/MyTextInput'
import ToolBar from '../../components/Toolbar'
import { color } from '../../styles/colors'
import RNPickerSelect from 'react-native-picker-select';
import { useNavigation } from '@react-navigation/native'
import { AuthContex } from '../../context/UsuarioContext'

export default function AnuncioScreen(props) {

    const { params } = props.route;
    const navigator = useNavigation();
    const { anuncios, editAnuncio } = useContext(AuthContex)
    return (
        <ScrollView style={{ flex: 1 }}>
            <StatusBar
                backgroundColor={color.WHITE}
                barStyle='dark-content'
                translucent={true}
            />
            <ToolBar titulo='Anunciobh'
                onPressLeft={() => goToBackScreen()}
                iconLeft={require('../../sources/img/back.png')}
            />

            <Formik
                validateOnMount={true}
                //validationSchema={loginValidationSchema}
                initialValues={{ title: params.anuncio.title, category: params.anuncio.category, subcategory: params.anuncio.subcategory, quantity: params.anuncio.quantity, rank: params.anuncio.rank, type: params.anuncio.type, comment: params.anuncio.comment, id:  params.anuncio.id }}
                onSubmit={values => editAnuncio(values, anuncios) } >

                {({
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    values,
                    errors,
                    touched,
                    isValid,
                }) => (
                    <>
                        <MyTextInput placeholder='Titulo' image='user'
                            onChangeText={handleChange('title')}
                            onBlur={handleBlur('title')}
                            value={values.title}

                        />

                        <MyTextInput placeholder='Categoria' image='user'
                            onChangeText={handleChange('category')}
                            onBlur={handleBlur('category')}
                            value={values.category}
                        />


                        <MyTextInput placeholder='Cantidad' image='user'
                            onChangeText={handleChange('quantity')}
                            onBlur={handleBlur('quantity')}
                            value={values.quantity}
                        />


                        <MyTextInput placeholder='Rango'
                            image='envelope'
                            onChangeText={handleChange('rank')}
                            onBlur={handleBlur('rank')}
                            value={values.rank}
                        />
                        <Text style={{ marginLeft: 20 }}>Tipo de producto</Text>
                        <RNPickerSelect
                            onValueChange={handleChange('type')}
                            value={values.type}
                            placeholder={{
                                label: 'Selecione',
                                value: '',
                            }}
                            items={[
                                { key: 1, label: 'herramienta', value: 'herramienta' },
                                { key: 2, label: 'repuesto', value: 'repuesto' },
                            ]}
                        />
                        <View style={styles.textAreaContainer} >

                            <TextInput
                                placeholder='Comentario'
                                style={styles.textArea}
                                numberOfLines={4}
                                multiline={true}
                                onChangeText={handleChange('comment')}
                                onBlur={handleBlur('comment')}
                                value={values.comment}
                            />
                        </View>

                        <View style={{ flexDirection: "row", marginTop: "10%" }}>
                            <MyButtonHome
                                titulo='Cancelar'
                                onPress={() => goToBackScreen()}
                            />
                            <MyButtonHome
                                titulo='Guardar'
                                onPress={() => handleSubmit()}
                            />
                        </View>
                    </>
                )}

            </Formik>


            { /*params.profile.familares.map(n => (
                <View key={n.id}>

                    <Text style={{ marginLeft: 20, marginTop: 30, fontFamily: 'summernote', fontSize: 25 }}>Familiar:</Text>
                    <Text style={{ marginLeft: 20, marginTop: 5, fontFamily: 'summernote', fontSize: 20 }}>{n.nombre + '\n' + n.numero}</Text>
                </View>

            )) */}



        </ScrollView>
    )
    function goToBackScreen() {
        navigator.goBack()
    }
}
export const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: "100%",
        backgroundColor: "white",
    }, containerTable: {
        marginTop: "5%",
        flexDirection: 'row',
        marginLeft: '-5%',
        alignItems: "center",
        justifyContent: "center"
    },
    touch: {
        flex: 1,
        justifyContent: "center",
        paddingHorizontal: 10,
        marginLeft: 5,
    },
    buttons: {
        alignItems: "center",
        backgroundColor: "#C9C3C3",
        padding: 5,
        borderRadius: 10
    }, CollapsibleViewStyle: {
        borderRadius: 5,
        borderColor: "white",
        borderWidth: 1,
    }, CollapsibleTitle: {
        backgroundColor: "red",
        marginLeft: "1%",
        fontSize: 23,
        borderRadius: 5,
        color: "white",
        width: "100%",
        padding: 20,
        shadowColor: "black",
    },
    textAreaContainer: {
        borderColor: color.BLACK,
        borderWidth: 0.7,
        alignContent: 'center',
        marginTop: 10,
        width: "100%"
    },
    textArea: {
        height: 100,
        justifyContent: "flex-start"
    },
    PickerStyle: {
        flex: 1,
        paddingTop: 5,
        marginLeft: "5%"
    }
});
