import React, { useState, useContext, useEffect } from 'react'
import { Text, View, TouchableOpacity, StatusBar, Image } from 'react-native'
import { loginStyles } from '../styles/styles'
import MyTextInput from '../components/MyTextInput'
import MyButton from '../components/MyButton'
import { AuthContex } from '../context/UsuarioContext'
import {color} from '../styles/colors'
import { useNavigation } from '@react-navigation/native';

export default function LoginScreen(props) {

    //const {singIn} = useContext(AuthContex)

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [hiddePassword, setHiddePassword] = useState(true)
    const navigator = useNavigation()


    return (
        <View style={[loginStyles.container]}>
            <StatusBar backgroundColor={color.WHITE} translucent={true} barStyle='dark-content' />
            <View style={[loginStyles.logo]}>
                <Image source={require('../sources/img/favicon.gif')}
                    style={{ height: 50, width: 200, marginTop: 30 }} />
            </View>
            <MyTextInput keyboardType='email-address' placeholder='E-mail' image='user'
                value={email} onChangeText={(email) => setEmail(email)} 
            />
            <MyTextInput
                keyboardType={null}
                placeholder='Password'
                image='lock'
                bolGone={true}
                secureTextEntry={hiddePassword}
                onPressIcon={() => setHiddePassword(!hiddePassword)}
                value={password} onChangeText={(password) => setPassword(password)}
            />
            <MyButton
                titulo='Iniciar sesion'
               // onPress={() => singIn(email, password)}
            />
            <View>
                <TouchableOpacity onPress={() => goToScreen('RegisterScreen')}>
                    <Text style={[loginStyles.txtTransparent, { color:color.RED, fontSize:17,marginBottom:5, textDecorationLine: 'none' }]}>Crear Una Cuenta</Text>
                </TouchableOpacity>
            </View>
            
            <View>
                <TouchableOpacity onPress={() => goToScreen('RecoveryPasswordScreen')}>
                    <Text style={[loginStyles.txtTransparent, {  fontSize:15, textDecorationLine: 'none' }]}>Olvide Contraseña</Text>
                </TouchableOpacity>
            </View>

        </View>
    )

    function goToScreen(routeName) {
        navigator.navigate(routeName);
    }


}