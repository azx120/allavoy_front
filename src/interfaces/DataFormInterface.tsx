export interface DataForm {
    marcas:       Marca[];
    tipoVehiculo: Marca[];
    repuestos:    Repuesto[];
}

export interface Marca {
    id:         string;
    nombre:     string;
    created_at: Date;
    updated_at: Date;
}

export interface Repuesto {
    id:               string;
    nombre:           string;
    created_at:       Date;
    updated_at:       Date;
    marca_id:         string;
    tipo_vehiculo_id: string;
}
