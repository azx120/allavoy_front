import React, { useContext, useEffect, useState } from 'react'
import { Text, View, TouchableOpacity, StatusBar, Alert, BackHandler, ScrollView, Image, Modal } from 'react-native'
import { loginStyles } from '../../styles/styles'
import { AuthContex } from '../../context/UsuarioContext'
import ElementListNotificacion from '../../components/ElementListNotificacion'
import {color} from '../../styles/colors'
import Icon from 'react-native-vector-icons/dist/Feather';
import CajaNotificaciones from '../../components/CajaNotificaciones'
import { useNavigation } from '@react-navigation/native';


export default function NotificationScreen(props) {

        const[modalOpen, setModalOpen] = useState(false);

        const goToScreen = (routeName) => {
            navigator.navigate(routeName)
        }
    return (
        <ScrollView>
            <View style={{ flex: 1,}}>

                <Image
                    source={ require('../../sources/img/ads.jpg') }
                    style={{ width: "100%", height:250,}}
                />
                <View style={{backgroundColor:color.SECONDARYCOLOR, padding:15, flexDirection: "row",flexWrap: "wrap",}}>
                    <View style={{ width:"90%", flexDirection: "row",flexWrap: "wrap", }}>
                        <Icon name='bell' size={35} color="#000"  />
                        <Text style={{fontSize:25,color:color.BLACK,fontWeight:"bold"}}
                        onPress={() => { goToScreen('RegisterScreen') }} >
                            Notificaciones
                        </Text>
                    </View>
                    <View>
                        <Icon name='menu' size={35} color="#000" />
                    </View>
                </View>
                <CajaNotificaciones />
                <CajaNotificaciones />
                <CajaNotificaciones />
                <CajaNotificaciones />
                <CajaNotificaciones />
                <CajaNotificaciones />
                <CajaNotificaciones />
            </View>
        </ScrollView>
    )
   

}