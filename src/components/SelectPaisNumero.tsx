import React ,{ useState  }from "react";
import { View, StyleSheet } from "react-native";
import { color } from "../styles/colors";
import RNPickerSelect from 'react-native-picker-select';
//import { reduce } from "core-js/core/array";


function SelectPaisNumero(props){

	const [ onValueChange, setOnValueChange ] = useState("");

    return(

		<View style={styles.container}>
			<RNPickerSelect	
				value={props.value}
				placeholder={{
					label: 'selecione',
					value: 'Selecionar',
				}}
				onValueChange={props.onValueChange}
				items={[
					{ label: 'Ecuador', value: '593' },
					{ label: 'Venezuela', value: '58' },
				]}


			/>
		</View>


    )


}

const styles = StyleSheet.create({
	container:{
		color:color.PRIMARYCOLOR,
		borderColor:color.PRIMARYCOLOR,
		borderWidth: 1,
		borderTopLeftRadius: 4,
		borderTopRightRadius: 4,
		borderBottomLeftRadius: 4,
		borderBottomRightRadius: 4,
	},
});

export default SelectPaisNumero