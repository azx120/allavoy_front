import React from "react"
import { color } from "../styles/colors"
import Icon from 'react-native-vector-icons/dist/Feather'
import { SearchBar } from 'react-native-elements'

function Busqueda(){

    return(

        <SearchBar
            placeholder="Buscar"
            onChangeText={this.updateSearch}
        />

    )

}

export default Busqueda