import React, { useState } from 'react'
import {
    Text,
    View,
    TouchableOpacity,
    StatusBar,
    ScrollView
} from 'react-native'
import { mainStyles, registroStyles } from '../../styles/styles'
import MyTextInput from '../../components/MyTextInput'
import ToolBar from '../../components/Toolbar'
import {color} from '../../styles/colors'
import { CheckBox, SocialIcon, Button } from 'react-native-elements'
import { useNavigation } from '@react-navigation/native'


export default function ChangePasswordScreen(props) {
    const navigator = useNavigation()
    const [hidePassword, setHidePassword] = useState(false)

    return (
        <View style={{ flex: 1}}>
            <ScrollView
                keyboardDismissMode='on-drag'
                keyboardShouldPersistTaps='always'
                style={{ backgroundColor: color.WHITE }}>
                <StatusBar backgroundColor={color.WHITE} translucent={true} />
                <ToolBar titulo='Cambiar Contraseña'
                    onPressLeft={() => navigator.goBack()}
                    iconLeft={require('../../sources/img/back.png')} />

                <View style={[mainStyles.container, { padding: 10 }]}>
                    
                    <MyTextInput keyboardType={null} placeholder='Vieja Contraseña'
                        onPressIcon={() => setHidePassword(!hidePassword)}
                        image='lock' bolGone={true}
                        secureTextEntry={hidePassword}
                    />
                    <MyTextInput keyboardType={null} placeholder='Nueva Contraseña'
                        onPressIcon={() => setHidePassword(!hidePassword)}
                        image='lock' bolGone={true}
                        secureTextEntry={hidePassword}
                    />
                    <MyTextInput keyboardType={null} placeholder='Confirmar Nueva Contraseña'
                        onPressIcon={() => setHidePassword(!hidePassword)}
                        image='lock' bolGone={true}
                        secureTextEntry={hidePassword}
                    />
                   
                    <View style={mainStyles.btnMain}>
                        <TouchableOpacity onPress={() =>{}}>
                            <Text style={mainStyles.btntxt}>Guardar Cambios</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                        <Button title="Cancelar" onPress={() => navigator.goBack()} type="clear" />
                    </View>
                </View>
            </ScrollView>
        </View>
    )

}