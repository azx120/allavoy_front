import React, { useEffect, useState, useContext } from 'react'
import { Text, View, StatusBar, Alert, TouchableOpacity, Image, StyleSheet, ScrollView, Button } from 'react-native'
import { loginStyles } from '../../styles/styles'
import MyTextInput from '../../components/MyTextInput'
import { Input } from 'react-native-elements'
import { AuthContex } from '../../context/UsuarioContext'
import ButtonAlarm from '../../components/ButtonAlarm'
import ButtonSolicitud from '../../components/ButtonSolicitud'
import { color } from '../../styles/colors'
import { showBottomAlert, useRefBottomAlert, BottomAlert } from 'react-native-modal-bottom-alert';
import MyButton from '../../components/MyButton'
import AgregarProducto from '../../components/AgregarProducto'
import EliminarProducto from '../../components/EliminarProducto'
import { Formik } from 'formik';
import SelectBuscador from '../../components/SelectBuscador'


export default function HomeClientScreen(props) {
    

    const { dataForm, createAnuncio, user } = useContext(AuthContex);   
  
/*
    */
    return (

        <ScrollView>
            <View style={{ flex: 1, paddingBottom: 20 }}>
                <StatusBar
                    backgroundColor={color.WHITE}
                    barStyle='dark-content'
                    translucent={true}
                />
                <Image
                    source={require('../../sources/img/ads.jpg')}
                    style={{ width: "100%", height: 250, }}
                />
                <View style={{ backgroundColor: color.SECONDARYCOLOR, justifyContent: "center", alignItems: "center" }}>
                    <Text style={{ fontSize: 25, padding: 10, color: color.BLACK, fontWeight: "bold" }}>
                        ¿Que Estas Buscando?
                    </Text>
                </View>

                <View style={styles.InputsHomeClient}>

                    <Formik
                        validateOnMount={true}
                        //validationSchema={loginValidationSchema}

                        initialValues={{ name:'', type:'', mark:'', comment:'', city:'', quantity:'', unity:'',user_id:'' }}
                        onSubmit={values  =>{ values.user_id = user['id'];  createAnuncio(values)}}
                        onReset= {(values, { resetForm }) => resetForm()}
                        >

                        {({
                            handleChange,
                            handleBlur,
                            handleSubmit,
                            handleReset,
                            values,
                            errors,
                            touched,
                            isValid,
                        }) => (
                            <>
                                <View style={styles.Margin}>
                                    <MyTextInput
                                        keyboardType='Text'
                                        placeholder='Ciudad'
                                        image='map-pin'
                                        onChangeText={handleChange('city')}
                                        onBlur={handleBlur('city')}
                                        value={values.city}
                                    />
                                </View>
                                <View style={styles.Margin}>
                                    <Text style={{ fontSize: 16, textAlign: "center" }}>
                                        Tipo de Vehiculo
                                </Text>
                                    <SelectBuscador
                                        usedata="tipoVehiculo"
                                        dataForm={dataForm}
                                        onValueChange={handleChange('type')}
                                        value={values.type}
                                    />
                                </View>
                                <View style={styles.Margin}>
                                    <Text style={{ fontSize: 16, textAlign: "center" }}>
                                        Marca
                                </Text>
                                    <SelectBuscador
                                        usedata="marcas"
                                        dataForm={dataForm}
                                        onValueChange={handleChange('mark')}
                                        value={values.mark}
                                    />
                                </View>
                                <View style={styles.Margin}>
                                    <MyTextInput
                                        keyboardType='Text'
                                        placeholder='Accesorio o Repuesto'
                                        image='shopping-cart'
                                        onChangeText={handleChange('name')}
                                        onBlur={handleBlur('name')}
                                        value={values.name}
                                    />
                                </View>
                                <View style={styles.Margin}>
                                    <MyTextInput
                                        keyboardType='Text'
                                        placeholder='Descripción Detallada:'
                                        image='file-text'
                                        onChangeText={handleChange('comment')}
                                        onBlur={handleBlur('comment')}
                                        value={values.comment}
                                    />
                                </View>
                                <View style={{
                                    flexDirection: "row",
                                    flexWrap: "wrap",
                                    marginVertical: 10
                                }}>
                                    <View style={{ width: "45%" }}>
                                        <MyTextInput
                                            keyboardType='Number'
                                            placeholder='Cantidad'
                                            image='plus'
                                            onChangeText={handleChange('quantity')}
                                            onBlur={handleBlur('quantity')}
                                            value={values.quantity}
                                        />
                                    </View>
                                    <View style={{ width: '10%' }}></View>
                                    <View style={{ width: "45%" }}>
                                        <MyTextInput
                                            keyboardType='Number'
                                            placeholder='Unidad'
                                            image='plus'
                                            onChangeText={handleChange('unity')}
                                            onBlur={handleBlur('unity')}
                                            value={values.unity}
                                        />
                                    </View>

                                </View>
                                <Button
                                    onPress={() => { }}
                                    title="Adjuntar Imagen"
                                    color="#e3ad33"
                                />

                                <View >
                                    <Text style={styles.Añadir}>
                                        + Añadir otro producto (Hasta 10 Productos)
                        </Text>
                                </View>
                                <TouchableOpacity style={{ marginTop: 20, alignItems: "center" }}
                                    onPress={() => handleSubmit()}>
                                    <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>


                                        <ButtonSolicitud />

                                    </View></TouchableOpacity>
                            </>
                        )}

                    </Formik>
                </View>
                <View style={{
                    flex: 1,
                    paddingTop: 15,
                    paddingBottom: 15,
                    paddingLeft: 10,
                    backgroundColor: color.PRIMARYCOLOR,
                    flexDirection: "row",
                    flexWrap: "wrap",
                }}>
                    <View style={{
                        width: "75%"
                    }}>
                        <Text
                            style={{
                                fontSize: 20,
                                color: color.WHITE
                            }}>
                            Hola mundo
                        </Text>
                    </View>
                    <View style={{
                        flexDirection: "row",
                        flexWrap: "wrap",
                        width: "25%",
                        justifyContent: "center"
                    }}>
                        <View style={{
                            width: "45%",
                            marginRight: "5%"
                        }}>
                            <AgregarProducto />
                        </View>
                        <View style={{
                            width: "45%",
                            marginLeft: "5%",
                        }}>
                            <EliminarProducto />
                        </View>
                    </View>
                </View>

            </View>
        </ScrollView>



    )



    /*function onOpenAlert() {
        showBottomAlert('info', 'This Title', 'This Message Example Info', () => onDonePress("mensaje 1"))
    };
    function onOpenAlertButtonPressed(titulo, input) {
        // onOpenAlert parameters: type, title, message, function
        // parameter type: 'success', 'info', 'error'
        // parameter title: 'string'
        // parameter message: 'string'
        // parameter action: function, default null'

        showBottomAlert('info', "Alarma de " + titulo,
            input,
            (e) => console.log(hand()))
    }
    function onDonePress(mensaje) {
        alert(mensaje);
        console.log(mensaje)
    }*/
}

const styles = StyleSheet.create({
    InputsHomeClient: {
        paddingLeft: "12%",
        paddingRight: "12%"
    },
    Añadir: {
        color: color.SECONDARYCOLOR,
        fontWeight: "bold",
        marginTop: 30,
        marginBottom: 30,
        textAlign: "center"
    },
    Margin: {
        marginVertical: 10
    }
});
