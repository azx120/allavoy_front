import React, { useContext, useEffect } from 'react'
import { Text, View, TouchableOpacity, StatusBar, Alert, BackHandler, Image } from 'react-native'
import Icon from 'react-native-vector-icons/dist/Feather';
import { DataTable } from 'react-native-paper';
import { AuthContex } from '../../context/UsuarioContext'
import MyButton from '../../components/MyButton'
import {color} from '../../styles/colors'
import { useNavigation } from '@react-navigation/native';

function useBackButton(handler) {
    useEffect(() => {
        BackHandler.addEventListener("hardwareBackPress", handler)
        return () => {
            console.log("hardwareBackPress close")
            BackHandler.removeEventListener("hardwareBackPress", handler)
        }
    }, [handler])
}

export default function SettingScreen(props) {
    //useBackButton(cerrarSesion)


    const navigator = useNavigation()
    const {user,logOut, access_token, status} = useContext(AuthContex)


    return (
        <View style={{ flex: 1, alignItems: 'center' }}>
            <StatusBar
                backgroundColor={color.WHITE}
                barStyle='dark-content'
                translucent={true}
            />
            {console.log(user)}
            <Text style={{ textAlign: 'center', marginTop: 80, fontFamily: 'summernote' }}>CONFIGURACION DE CUENTA {'\n' + status} </Text>

            <DataTable>

                <TouchableOpacity onPress={() => goToScreen("EditPerfilScreen")} >

                    <DataTable.Row style={{ height: 70, padding: 10 }}>
                        <DataTable.Cell style={{ flex: 0.3 }}>
                            <View>
                                <Icon size={30} color="grey" name={"user"} />
                            </View>

                        </DataTable.Cell>
                        <DataTable.Cell style={{ flex: 2 }}>
                            <Text style={{ fontSize: 20 }}> Editar Perfil </Text>
                        </DataTable.Cell>

                        <DataTable.Cell style={{ flex: 0.3 }}>
                            <View>
                                <Image source={require('../../sources/img/next.png')} style={{ height: 15, width: 15 }} />
                            </View>
                        </DataTable.Cell>

                    </DataTable.Row>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => goToScreen("ChangePasswordScreen")} >

                    <DataTable.Row style={{ height: 70, padding: 10 }}>
                        <DataTable.Cell style={{ flex: 0.3 }}>
                            <View>
                                <Icon size={30} color="grey" name={"shield"} />
                            </View>

                        </DataTable.Cell>
                        <DataTable.Cell style={{ flex: 2 }}>
                            <Text style={{ fontSize: 20 }} > Cambiar Contraseña </Text>
                        </DataTable.Cell>

                        <DataTable.Cell style={{ flex: 0.3 }}>
                            <View>
                                <Image source={require('../../sources/img/next.png')} style={{ height: 15, width: 15 }} />
                            </View>
                        </DataTable.Cell>

                    </DataTable.Row>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => goToScreen("FamiliaresSettingScreen")}  >

                    <DataTable.Row style={{ height: 70, padding: 10 }}>
                        <DataTable.Cell style={{ flex: 0.3 }}>
                            <View>
                                <Icon size={30} color="grey" name={"users"} />
                            </View>

                        </DataTable.Cell>
                        <DataTable.Cell style={{ flex: 2 }}>
                            <Text style={{ fontSize: 20 }}> Editar Familiares/amigos </Text>
                        </DataTable.Cell>

                        <DataTable.Cell style={{ flex: 0.3 }}>
                            <View>
                                <Image source={require('../../sources/img/next.png')} style={{ height: 15, width: 15 }} />
                            </View>
                        </DataTable.Cell>

                    </DataTable.Row>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => goToScreen("SoundSettingScreen")}  >

                    <DataTable.Row style={{ height: 70, padding: 10 }}>
                        <DataTable.Cell style={{ flex: 0.3 }}>
                            <View>
                                <Icon size={30} color="grey" name={"volume-2"} />
                            </View>

                        </DataTable.Cell>
                        <DataTable.Cell style={{ flex: 2 }}>
                            <Text style={{ fontSize: 20 }} > Sonidos </Text>
                        </DataTable.Cell>

                        <DataTable.Cell style={{ flex: 0.3 }}>
                            <View>
                                <Image source={require('../../sources/img/next.png')} style={{ height: 15, width: 15 }} />
                            </View>
                        </DataTable.Cell>

                    </DataTable.Row>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => goToScreen("SettingNotificacion")}  >

                    <DataTable.Row style={{ height: 70, padding: 10 }}>
                        <DataTable.Cell style={{ flex: 0.3 }}>
                            <View>
                                <Icon size={30} color="grey" name={"bell"} />
                            </View>

                        </DataTable.Cell>
                        <DataTable.Cell style={{ flex: 2 }}>
                            <Text style={{ fontSize: 20 }} > Notificaciones </Text>
                        </DataTable.Cell>

                        <DataTable.Cell style={{ flex: 0.3 }}>
                            <View>
                                <Image source={require('../../sources/img/next.png')} style={{ height: 15, width: 15 }} />
                            </View>
                        </DataTable.Cell>

                    </DataTable.Row>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => goToScreen("IdiomaSettingScreen")}  >

                    <DataTable.Row style={{ height: 70, padding: 10 }}>
                        <DataTable.Cell style={{ flex: 0.3 }}>
                            <View>
                                <Icon size={30} color="grey" name={"message-circle"} />
                            </View>

                        </DataTable.Cell>
                        <DataTable.Cell style={{ flex: 2 }}>
                            <Text style={{ fontSize: 20 }} > Idioma </Text>
                        </DataTable.Cell>

                        <DataTable.Cell style={{ flex: 0.3 }}>
                            <View>
                                <Image source={require('../../sources/img/next.png')} style={{ height: 15, width: 15 }} />
                            </View>
                        </DataTable.Cell>

                    </DataTable.Row>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => goToScreen("HelpScreen")}  >

                    <DataTable.Row style={{ height: 70, padding: 10 }}>
                        <DataTable.Cell style={{ flex: 0.3 }}>
                            <View>
                                <Icon size={30} color="grey" name={"info"} />
                            </View>

                        </DataTable.Cell>
                        <DataTable.Cell style={{ flex: 2 }}>
                            <Text style={{ fontSize: 20 }}> Ayuda </Text>
                        </DataTable.Cell>

                        <DataTable.Cell style={{ flex: 0.3 }}>
                            <View>
                                <Image source={require('../../sources/img/next.png')} style={{ height: 15, width: 15 }} />
                            </View>
                        </DataTable.Cell>

                    </DataTable.Row>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => cerrarSesion()}  >

                    <DataTable.Row style={{ height: 70, padding: 10 }}>
                        <DataTable.Cell style={{ flex: 0.3 }}>
                            <View>
                                <Icon size={30} color="grey" name={"log-out"} />
                            </View>

                        </DataTable.Cell>
                        <DataTable.Cell style={{ flex: 2 }}>
                            <Text style={{ fontSize: 20 }} > Cerrar Sesión </Text>
                        </DataTable.Cell>

                        <DataTable.Cell style={{ flex: 0.3 }}>
                            <View>

                            </View>
                        </DataTable.Cell>

                    </DataTable.Row>
                </TouchableOpacity>

            </DataTable>
        </View>

    )
    function cerrarSesion() {
        Alert.alert("Salir", "Seguro de \n Salir de La Sesion?",
            [
                {
                    text: "Si", onPress: () => {
                        logOut()
                        //,goToScreen('LoginScreen')
                    }
                },
                {
                    text: "No", onPress: () => { }, style: 'cancel'
                }
            ]
        )
    }

    function goToScreen(routeName) {
        navigator.navigate(routeName);
    }


}