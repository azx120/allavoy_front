import React, { useContext, useState } from 'react'
import { Text, View, TouchableOpacity, StatusBar, Alert, ScrollView } from 'react-native'
import ToolBar from '../../components/Toolbar'
import { AuthContex } from '../../context/UsuarioContext'
import ElementListAnuncios from '../../components/ElementListAnuncios'
import { color } from '../../styles/colors'
import { useNavigation } from '@react-navigation/native'



export default function AnunciosScreen(props) {
    const navigator = useNavigation()
    const {anuncios} = useContext(AuthContex)
 

    return (
        <ScrollView style={{ flex: 1 }}>
            <StatusBar
                backgroundColor={color.WHITE}
                barStyle='dark-content'
                translucent={true}
            />
            <ToolBar titulo='Anuncios'
                onPressLeft={() => goToBackScreen()}
                iconLeft={require('../../sources/img/back.png')}
                onPressRight={() => goToScreen('AnuncioScreen')}
                iconRight={require('../../sources/img/plus.png')}
            />

            <View >
                <ElementListAnuncios
                    navigation={props.navigation}
                    anuncios={anuncios}
                />
            </View>



        </ScrollView>

    )
    function goToScreen(routeName) {
        navigator.navigate(routeName);
    }
    function goToBackScreen() {
        navigator.goBack()
    }

}