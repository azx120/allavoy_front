import { Anuncio } from './AnuncioInterface';
import { DataForm } from './DataFormInterface';

export interface confirmNumberResponse {
    status:       number;
    access_token: string;
    token_type:   string;
    expires_at:   Date;
    user:         User;
    anuncios:      Anuncio[];
    dataForm : DataForm[];

}

export interface sendCodeResponse {
    status:       number;

}

export interface RegisterResponse {
    status:       number;
    token_type:   string;
    expires_at:   Date;
    user:         User;
    anuncios:      Anuncio[];
    dataForm : DataForm[];
}

export interface User {
    id:                string;
    name:              null;
    email:             null;
    phone:             string;
    dni:               null;
    birth:             null;
    rol:               string;
    email_verified_at: null;
    phone_verified:    number;
    register_verified: number;
    code_phone:        string;
    deleted_at:        null;
    created_at:        Date;
    updated_at:        Date;
}
