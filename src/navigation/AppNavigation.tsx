import React, { useContext } from 'react'
import { createStackNavigator } from '@react-navigation/stack';

import SplashScreen from '../screens/SplashScreens'
import { SliderHomeScreen } from '../screens/HomeScreens'
import PrincipalScreens from '../screens/PrincipalScreens'
import RegisterScreen from '../screens/RegisterScreen'
import IngresarNumeroScreen from '../screens/IngresarNumeroScreen'

import ChangePasswordScreen from '../screens/systemScreen/ChangePasswordScreen'
import EditPerfilScreen from '../screens/systemScreen/EditPerfilScreen'
import SettingNotificacion from '../screens/systemScreen/SettingNotificationScreen'
import FamiliaresSettingScreen from '../screens/systemScreen/FamiliarSettingScreen'
import HelpScreen from '../screens/systemScreen/HelpScreen'
import VecinosGrupoScreen from '../screens/systemScreen/VecinosGrupoScreen'
import VecinoScreen from '../screens/systemScreen/VecinoScreen'
import SoundSettingScreen from '../screens/systemScreen/SoundSettingScreen'
import IdiomaSettingScreen from '../screens/systemScreen/IdiomaSettingScreen'
import GrupoScreens from '../screens/systemScreen/GrupoScreens'
import AnunciosScreen from '../screens/systemScreen/AnunciosScreen'
import AnuncioScreen from '../screens/systemScreen/AnuncioScreen'
import EditAnuncioScreen from '../screens/systemScreen/EditAnuncioScreen'
import FavoriteScreen from '../screens/systemScreen/FavoriteScreen'
import { AuthContex } from '../context/UsuarioContext';
import ValidacionNumeroScreen from '../screens/ValidacionNumeroScreen';
import Chat from '../screens/systemScreen/Chat'



const Stack = createStackNavigator();

export const Navigation = () => {

    const { status } = useContext(AuthContex)

    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false,
                cardStyle: {
                    backgroundColor: '#ffff'
                }
            }}
        >
            {
                (function () {

                    if (status === 'authenticated') {
                        return (
                            <>
                                <Stack.Screen name="PrincipalScreen" component={PrincipalScreens} />
                                <Stack.Screen name="ChangePasswordScreen" component={ChangePasswordScreen} />
                                <Stack.Screen name="EditPerfilScreen" component={EditPerfilScreen} />
                                <Stack.Screen name="SettingNotificacion" component={SettingNotificacion} />
                                <Stack.Screen name="FamiliaresSettingScreen" component={FamiliaresSettingScreen} />
                                <Stack.Screen name="VecinosGrupoScreen" component={VecinosGrupoScreen} />
                                <Stack.Screen name="VecinoScreen" component={VecinoScreen} />
                                <Stack.Screen name="SoundSettingScreen" component={SoundSettingScreen} />
                                <Stack.Screen name="IdiomaSettingScreen" component={IdiomaSettingScreen} />
                                <Stack.Screen name="GrupoScreens" component={GrupoScreens} />
                                <Stack.Screen name="AnunciosScreen" component={AnunciosScreen} />
                                <Stack.Screen name="AnuncioScreen" component={AnuncioScreen} />
                                <Stack.Screen name="EditAnuncioScreen" component={EditAnuncioScreen} />
                                <Stack.Screen name="FavoriteScreen" component={FavoriteScreen} />
                                <Stack.Screen name="HelpScreen" component={HelpScreen} />
                                <Stack.Screen name="Chat" component={Chat} />
                            </>
                           
                        )
                    } else if (status === 'registered-phone') {
                        return (
                            <Stack.Screen name="RegisterScreen" component={RegisterScreen} />
                        )
                    } else {
                        return (
                            <>
                            <Stack.Screen name="SplashScreen" component={SplashScreen} />
                            <Stack.Screen name="SliderHomeScreen" component={SliderHomeScreen} />
                            <Stack.Screen name="IngresarNumeroScreen" component={IngresarNumeroScreen} />
                            <Stack.Screen name="ValidacionNumeroScreen" component={ValidacionNumeroScreen} />
                        </>
                        )
                    }



                })()
            }


        </Stack.Navigator>
    );
}

