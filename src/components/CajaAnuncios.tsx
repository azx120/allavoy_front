import React, { useContext, useEffect } from 'react'
import { Text, View, TouchableOpacity, StatusBar, Alert, BackHandler, ScrollView, Image } from 'react-native'
import {color} from '../styles/colors'
import Icon from 'react-native-vector-icons/dist/Feather'

function CajaAnuncios(){

    return(
        <View style={{
            marginTop:10,
            backgroundColor:color.PRIMARYCOLOR,
            width:"100%",
            padding:10
        }}>

            <View style={{
                flexDirection: "row",
                flexWrap: "wrap",
                width:"80%"
            }}
            >
                <Text style={{
                    color:color.WHITE,
                    fontWeight:"bold",
                    fontSize:20,

                }}>
                    Gey=
                </Text>
                <Text style={{
                    color:color.WHITE,
                    fontWeight:"bold",
                    fontSize:20,

                }}>
                    Unidad. /
                </Text>
                <Text style={{
                    color:color.WHITE,
                    fontWeight:"bold",
                    fontSize:20,

                }}>
                    Filtro generico. /
                </Text>
                <Text style={{
                    color:color.WHITE,
                    fontWeight:"bold",
                    fontSize:20,

                }}>
                    Moto . /
                </Text>
                <Text style={{
                    color:color.WHITE,
                    fontWeight:"bold",
                    fontSize:20,

                }}>
                    Descripcion
                </Text>
            </View>
            <View style={{
                flexDirection: "row",
                flexWrap: "wrap",
                paddingTop:5
            }}>
                <View style={{
                    width:"65%",
                }}>
                    <Text style={{
                        color:color.SECONDARYCOLOR,
                        fontSize:17,
                        fontWeight:"bold"
                    }}>
                        Dia, Hora
                    </Text>
                </View>
                <View style={{
                    width:"35%",
                }}>
                    <Text style={{
                        color:color.SECONDARYCOLOR,
                        fontSize:17,
                        fontWeight:"bold"
                    }}>
                        Respuestas, 100
                    </Text>
                </View>
            </View>

        </View>
    )

}

export default CajaAnuncios