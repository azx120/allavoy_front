import { useNavigation } from '@react-navigation/native'
import React, { useContext, Component } from 'react'
import { View, TouchableOpacity, Image, StyleSheet, Text, StatusBar, ScrollView } from 'react-native'
import MyButton from '../../components/MyButton'
import ToolBar from '../../components/Toolbar'
import {color} from '../../styles/colors'

export default function VecinoScreen(props) {
    const navigator = useNavigation()
    const { params } = props.route;
    const familiarCount = 0;
    return (
        <ScrollView style={{ flex: 1 }}>
            <StatusBar
                backgroundColor={color.WHITE}
                barStyle='dark-content'
                translucent={true}
            />
            <ToolBar titulo='Vecino'
                onPressLeft={() => goToBackScreen()}
                iconLeft={require('../../sources/img/back.png')}
            />

            <Text style={{ textAlign: 'center', marginTop: 10, fontFamily: 'summernote', fontSize: 30 }}>{params.profile.name} {params.profile.lastname}</Text>

            <Text style={{ marginLeft: 20, marginTop: 30, fontFamily: 'summernote', fontSize: 25 }}>Direccion:</Text>
            <Text style={{ marginLeft: 20, marginTop: 5, fontFamily: 'summernote', fontSize: 20 }}>{params.profile.address}</Text>

            <Text style={{ marginLeft: 20, marginTop: 30, fontFamily: 'summernote', fontSize: 25 }}>Teléfono:</Text>
            <Text style={{ marginLeft: 20, marginTop: 5, fontFamily: 'summernote', fontSize: 20 }}>{params.profile.phone}</Text>


            {params.profile.familares.map(n => (
                <View key={n.id}>

                    <Text style={{ marginLeft: 20, marginTop: 30, fontFamily: 'summernote', fontSize: 25 }}>Familiar:</Text>
                    <Text style={{ marginLeft: 20, marginTop: 5, fontFamily: 'summernote', fontSize: 20 }}>{n.nombre + '\n' + n.numero}</Text>
                </View>

            ))}
            <View style={{ alignItems: 'center', marginTop: 20 }}>
                <Image source={require('../../sources/img/map.png')} />
                <MyButton titulo='Silenciar' />
            </View>


        </ScrollView>
    )
    function goToBackScreen() {
        navigator.goBack()
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: "100%",
        backgroundColor: "white",
    }, containerTable: {
        marginTop: "5%",
        flexDirection: 'row',
        marginLeft: '-5%',
        alignItems: "center",
        justifyContent: "center"
    },
    touch: {
        flex: 1,
        justifyContent: "center",
        paddingHorizontal: 10,
        marginLeft: 5,
    },
    buttons: {
        alignItems: "center",
        backgroundColor: "#C9C3C3",
        padding: 5,
        borderRadius: 10
    }, CollapsibleViewStyle: {
        borderRadius: 5,
        borderColor: "white",
        borderWidth: 1,
    }, CollapsibleTitle: {
        backgroundColor: "red",
        marginLeft: "1%",
        fontSize: 23,
        borderRadius: 5,
        color: "white",
        width: "100%",
        padding: 20,
        shadowColor: "black",
    }

});
