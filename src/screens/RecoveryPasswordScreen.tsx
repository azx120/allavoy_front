import React, { useState } from 'react'
import { Text, View, TouchableOpacity, StatusBar, ScrollView  } from 'react-native'
import { loginStyles } from '../styles/styles'
import MyTextInput from '../components/MyTextInput'
import ToolBar from '../components/Toolbar'
//import MyTextInput from '../components/MyButton'
import {color} from '../styles/colors'
import { useNavigation } from '@react-navigation/native';






export default function RecoveryPasswordScreen(props){

    const navigator = useNavigation()
    return(
         <View style={{ flex: 1}}>
        <ScrollView 
            keyboardDismissMode='on-drag'
            keyboardShouldPersistTaps='always'
            style={{backgroundColor: color.WHITE}}
        >
            <StatusBar backgroundColor={ color.WHITE } translucent={true} />
            <ToolBar titulo='Recuperar Contraseña' onPressLeft={()=> navigator.goBack()} iconLeft={require('../sources/img/back.png')} />
            <View style = { [loginStyles.container] }>
            <MyTextInput keyboardType='email-address' placeholder='E-mail' image='user' />
           
            <View style={loginStyles.btnMain}>
                <TouchableOpacity onPress={()=> goToScreen('LoginScreen')}>
                    <Text style={ loginStyles.btntxt }>Enviar Contraseña</Text>
                </TouchableOpacity>
            </View>
            
        </View>
        </ScrollView>
        </View>
    )
    function goToScreen(routeName){
        navigator.navigate(routeName);
    }
}