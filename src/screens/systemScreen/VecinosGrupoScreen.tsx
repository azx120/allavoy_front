import React, { useContext, Component } from 'react'
import { View, TouchableOpacity, Image, StyleSheet, Text, StatusBar, ScrollView } from 'react-native'
import { DataTable, List } from 'react-native-paper';
import ToolBar from '../../components/Toolbar'
import {color} from '../../styles/colors'
import Icon from 'react-native-vector-icons/dist/Feather';
import { useNavigation } from '@react-navigation/native';

export default function VecinosGrupoScreen(props) {
    const navigator = useNavigation()
    const { params } =props.route;
    return (
        <ScrollView style={{ flex: 1 }}>
            <StatusBar
                backgroundColor={color.WHITE}
                barStyle='dark-content'
                translucent={true}
            />
            <ToolBar titulo='Integrantes'
                onPressLeft={() => goToBackScreen()}
                iconLeft={require('../../sources/img/back.png')}
            />
            <DataTable>
                {params.integrantes.map(n => (

                    <TouchableOpacity key={n.id} onPress={() => goToScreen('VecinoScreen', n)}>

                        <DataTable.Row style={{ height: 70, padding: 10 }}>
                            <DataTable.Cell style={{ flex: 0.3 }}>
                                <View>
                                    <Icon size={20} color="grey" name={"user"} />
                                </View>

                            </DataTable.Cell>
                            <DataTable.Cell style={{ flex: 3 }}>

                                <Text style={{ fontSize: 15 }}>    {n.name}</Text>


                            </DataTable.Cell>

                            <DataTable.Cell style={{ flex: 0.3 }}>
                                <View>
                                    <Image source={require('../../sources/img/next.png')} style={{ height: 15, width: 15 }} />
                                </View>

                            </DataTable.Cell>

                        </DataTable.Row>
                    </TouchableOpacity>
                ))}
            </DataTable>
        </ScrollView>
    )
    function goToScreen(routeName, data) {
        navigator.navigate(routeName as never, { profile: data } as never)
    }

    function goToBackScreen() {
        navigator.goBack()
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: "100%",
        backgroundColor: "white",
    }, containerTable: {
        marginTop: "5%",
        flexDirection: 'row',
        marginLeft: '-5%',
        alignItems: "center",
        justifyContent: "center"
    },
    touch: {
        flex: 1,
        justifyContent: "center",
        paddingHorizontal: 10,
        marginLeft: 5,
    },
    buttons: {
        alignItems: "center",
        backgroundColor: "#C9C3C3",
        padding: 5,
        borderRadius: 10
    }, CollapsibleViewStyle: {
        borderRadius: 5,
        borderColor: "white",
        borderWidth: 1,
    }, CollapsibleTitle: {
        backgroundColor: "red",
        marginLeft: "1%",
        fontSize: 23,
        borderRadius: 5,
        color: "white",
        width: "100%",
        padding: 20,
        shadowColor: "black",
    }

});
