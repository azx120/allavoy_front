import React from 'react'
import { Text, TouchableOpacity } from 'react-native'
import { HomeButton } from '../styles/styles'
import { color } from '../styles/colors'

function MyButtonHome(props){

    const sButton = props.transparent?HomeButton.btnTransparent:HomeButton.btnMain
    const sText = props.transparent?{ color: color.RED }: null

    return(
        <TouchableOpacity style={[sButton, props.styles]} onPress={props.onPress}>
            <Text style={[HomeButton.btntxt, sText]}>{props.titulo} </Text> 
        </TouchableOpacity>
    )
}
export default MyButtonHome