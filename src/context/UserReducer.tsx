import React, { createContext, useReducer, useState, useEffect } from 'react'
import { saveUsuario, deleteUsuario } from '../storage/UsuarioAsyncStorage'
import { saveAnuncios, deleteAnuncios } from '../storage/AnunciosAsyncStorage'
import {saveDataForm, getDataForm, deleteDataForm   } from '../storage/FormDataAsyncStorage'
import Snackbar from 'react-native-snackbar'
import { User } from '../interfaces/UserInterface';
import { Anuncio } from '../interfaces/AnuncioInterface';
import { DataForm } from '../interfaces/DataFormInterface';

export interface Authstate {
    status: 'cheking' | 'authenticated' | 'not-authenticated' | 'registered-phone' | 'registered-dates';
    access_token: string | null | '';
    user: User | null | '';
    errorMessage: string;
    numberTemporal: string;
    anuncios: Anuncio[] | null | '';
    dataForm: DataForm[] | null | '';


}

export type AuthAction =
    | { type: 'sing-in', payload: { user: User, anuncios: Anuncio[], dataForm: DataForm[] } }
    | { type: 'confirmedNumber', payload: { access_token: string, user: User, anuncios: Anuncio[], dataForm: DataForm[] } }
    | { type: 'sing-up', payload: { user: User, anuncios: Anuncio[], dataForm: DataForm[] } }
    | { type: 'sing-out', payload: { access_token: null, user: null } }
    | { type: 'addError', payload: string }
    | { type: 'removeError', payload: string }
    | { type: 'addErrorsistem', payload: string }
    | { type: 'notAuthenticated' }
    | { type: 'logaout' }
    | { type: 'numberTemporal', payload: string }
    | { type: 'createAnuncio', payload: { anuncios: Anuncio } }
    | { type: 'editAnuncio', payload: { anuncio: Anuncio, anuncios: Anuncio[] } }

export const userReducer = (state: Authstate, action: AuthAction): Authstate => {

    switch (action.type) {

        case 'sing-in':
            return { ...state, user: action.payload.user, status: (action.payload.user.register_verified == 0)?'registered-phone':'authenticated', access_token: action.payload.user["access_token"], anuncios: action.payload.anuncios,  dataForm: action.payload.dataForm }

        case 'confirmedNumber':
            action.payload.user["access_token"] = action.payload.access_token;

            saveUsuario(action.payload.user).then((msg) => {
                console.log('user save')
            })

           saveAnuncios(action.payload.anuncios).then((msg) => {
                console.log('anuncios save')
            })

            saveDataForm(action.payload.dataForm).then((msg) => {
                console.log('datos save')
            })

            Snackbar.show({
                text: 'Inicio de sesion exitoso',
                duration: Snackbar.LENGTH_LONG,
            })

            return {
                ...state,
                user: action.payload.user,
                status: (action.payload.user.register_verified == 0)?'registered-phone':'authenticated',
                access_token: action.payload.access_token,
                anuncios: action.payload.anuncios,
                dataForm: action.payload.dataForm
            }

        case 'sing-up':

            saveUsuario(action.payload.user).then((msg) => {
                console.log('user save')
            })

            Snackbar.show({
                text: 'Registro de usuario exitoso',
                duration: Snackbar.LENGTH_LONG,
            })
            return {
                ...state,
                user: action.payload.user,
                status: 'authenticated',
                anuncios: action.payload.anuncios,
                dataForm: action.payload.dataForm
            }


        case 'sing-out':

            deleteUsuario().then((msg) => {
            })
            Snackbar.show({
                text: 'sesion Expirada',
                duration: Snackbar.LENGTH_LONG,
            })
            return {
                ...state,
                user: '',
                status: 'not-authenticated',
                access_token: ''
            }
        case 'addError':
            Snackbar.show({
                text: action.payload,
                duration: Snackbar.LENGTH_LONG,
            })
            return {
                ...state,
                errorMessage: action.payload
            }
        case 'addErrorsistem':
            Snackbar.show({
                text: action.payload,
                duration: Snackbar.LENGTH_LONG,
            })
            return {
                ...state,
                errorMessage: action.payload
            }
        case 'removeError':
            return {
                ...state,
                errorMessage: ''
            }

        case 'numberTemporal':

            return {
                ...state,
                numberTemporal: action.payload
            }


        case 'notAuthenticated':
            return {
                ...state,
                errorMessage: 'not-authenticated',
                access_token: null,
                user: null
            }
        case 'createAnuncio':


            Snackbar.show({
                text: 'Ununcio registrado con exito',
                duration: Snackbar.LENGTH_LONG,
            }) 
            return {
                ...state}
        case 'editAnuncio':
            let newArray = action.payload.anuncios;
            const editAnuncio = action.payload.anuncio;

            newArray.forEach((data, key) => {
                if (data['id'] == editAnuncio['id']) {

                    newArray[key].subcategory = editAnuncio['subcategory'];
                    newArray[key].category = editAnuncio['category'];
                    newArray[key].quantity = editAnuncio['quantity'];
                    newArray[key].rank = editAnuncio['rank'];
                    newArray[key].comment = editAnuncio['comment'];

                    Snackbar.show({
                        text: 'Ununcio registrado con exito',
                        duration: Snackbar.LENGTH_LONG,
                    })
                    saveAnuncios(newArray).then((msg) => {
                        console.log('anuncios save')
                    })
                    return {

                        anuncios: newArray
                    }
                }
            });

        default:

            return state



    }
}