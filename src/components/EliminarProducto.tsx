import React from 'react'
import { Text, TouchableOpacity, StyleSheet } from 'react-native'



function EliminarProducto(){

    return(
        <TouchableOpacity>
                
            <Text style={styles.TextEliminar}>
                x
            </Text>

        </TouchableOpacity>
    )

}


const styles = StyleSheet.create({
    TextEliminar:{
        fontSize:28,
        lineHeight:28,
        fontWeight:"bold",
        color:"white"
    }
})


export default EliminarProducto