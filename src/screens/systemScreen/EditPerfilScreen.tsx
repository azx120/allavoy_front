import React, { useState } from 'react'
import {
    Text,
    View,
    TouchableOpacity,
    StatusBar,
    ScrollView
} from 'react-native'
import { mainStyles, registroStyles } from '../../styles/styles'
import MyTextInput from '../../components/MyTextInput'
import ToolBar from '../../components/Toolbar'
import {color} from '../../styles/colors'
import { CheckBox, SocialIcon, Button } from 'react-native-elements'
import { useNavigation } from '@react-navigation/native'


export default function EditPerfilScreen(props) {
    const navigator = useNavigation()
    const [hidePassword, setHidePassword] = useState(false)

    return (
        <View style={{ flex: 1}}>
            <ScrollView
                keyboardDismissMode='on-drag'
                keyboardShouldPersistTaps='always'
                style={{ backgroundColor: color.WHITE }}>
                <StatusBar backgroundColor={color.WHITE} translucent={true} />
                <ToolBar titulo='Editar Perfil'
                    onPressLeft={() => navigator.goBack()}
                    iconLeft={require('../../sources/img/back.png')} />

                <View style={[mainStyles.container, { padding: 10 }]}>
                    <Text style={mainStyles.titleText}>Datos De Usuario</Text>
                    <MyTextInput placeholder='Nombres' image='user' />
                    <MyTextInput placeholder='Apellidos' image='user' />
                    <MyTextInput keyboardType='email-address' placeholder='E-mail'
                        image='envelope' />
                    
                    <View style={mainStyles.btnMain}>
                        <TouchableOpacity onPress={() =>{}}>
                            <Text style={mainStyles.btntxt}>Guardar Cambios</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                        <Button title="Cancelar" onPress={() => navigator.goBack()} type="clear" />
                    </View>
                   
                </View>
            </ScrollView>
        </View>
    )

}