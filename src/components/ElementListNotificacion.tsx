import React, { useState } from 'react'
import { View, TouchableOpacity, Image, StyleSheet, Text } from 'react-native'
import { DataTable } from 'react-native-paper';
import Icon from 'react-native-vector-icons/dist/Feather';
import { loginStyles } from '../styles/styles'
import { color } from '../styles/colors'


const ElementListNotificacion = (props) => {

  const [isView, setIsView] = useState(false);
  const toggleSwitch = () => setIsView(previousState => !previousState);

  return (

    <DataTable>
      {props.vecinos.map(n => (


        <TouchableOpacity key={n.id} onPress={() => toggleSwitch}>
          <DataTable.Row style={{ height: 70, padding: 10, borderColor: "#0000", borderRadius:20}}>

            <DataTable.Cell style={{ flex: 0.3 }}>

              <View>
                <Image source={
                  //isView = n.type 
                  (n.type == 0) ? 
                    (n.view == 1) ? 
                      require('../sources/img/alarm-grey.png') : 
                      require('../sources/img/alarm.png') : 
                  (n.type == 1) ? 
                    (n.view == 1) ? 
                      require('../sources/img/salud-grey.png') : 
                      require('../sources/img/salud.png'):
                    (n.view == 1) ?   
                      require('../sources/img/fuego-grey.png'):
                      require('../sources/img/fuego.png')
                  } 
                  style={{ height: 25, width: 25 }} />
              </View>
            </DataTable.Cell>

            <DataTable.Cell style={{ flex: 2 }}>
              <View>
                <Text style={{ fontSize: 20 }}>{n.name}</Text>
                <View style={{flexDirection: 'row'}}>
                  <Icon size={15} color="black" name={"clock"} />
                  <Text style={{ fontSize: 13, marginLeft: 5 }}>{n.date}</Text>
                </View>
                
              </View>

            </DataTable.Cell>

            <DataTable.Cell style={{ flex: 0.3 }}>
              <View>
              <Icon size={22} color={props.colorIcon} name={"star"} />
              </View>
            </DataTable.Cell>

          </DataTable.Row>
        </TouchableOpacity>
      ))}
    </DataTable>

  );
  function viewNotification(id){
    const lol = props.vecinos.find(el => el.id === id)
    lol.view = 1
    alert(lol.view)
  }
};
const styles = StyleSheet.create({
  container: {
    marginTop: "5%",
    flexDirection: 'row',
    marginLeft: '-5%',
    alignItems: "center",
    justifyContent: "center"
  },
  touch: {
    flex: 1,
    justifyContent: "center",
    paddingHorizontal: 10,

  },
  buttons: {
    alignItems: "center",
    backgroundColor: "#C9C3C3",
    padding: 5,
    borderRadius: 10
  }

});


export default ElementListNotificacion;

