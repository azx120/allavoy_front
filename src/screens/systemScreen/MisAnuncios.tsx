import React, { useContext, useEffect } from 'react'
import { Text, View, TouchableOpacity, StatusBar, Alert, BackHandler, ScrollView, Image } from 'react-native'
import { loginStyles } from '../../styles/styles'
import { AuthContex } from '../../context/UsuarioContext'
import ElementListNotificacion from '../../components/ElementListNotificacion'
import {color} from '../../styles/colors'
import Icon from 'react-native-vector-icons/dist/Feather';
import CajaAnuncios from '../../components/CajaAnuncios'


export default function MisAnuncios(props) {


    //const [login, loginAction] = useContext(UserContext)
    return (    
        <ScrollView>
            <View style={{ flex: 1,}}>
                <Image
                    source={ require('../../sources/img/ads.jpg') }
                    style={{ width: "100%", height:250,}}
                />
                <View style={{backgroundColor:color.SECONDARYCOLOR, padding:15, flexDirection: "row",flexWrap: "wrap",}}>
                    <View style={{ width:"90%", flexDirection: "row",flexWrap: "wrap", }}>
                        <Icon name='folder' size={35} color="#000"  />
                        <Text style={{fontSize:25,color:color.BLACK,fontWeight:"bold"}}>
                            Anuncios
                        </Text>
                    </View>
                    <View>
                        <Icon name='menu' size={35} color="#000" />
                    </View>
                </View>
                <CajaAnuncios />
                <CajaAnuncios />
                <CajaAnuncios />
                <CajaAnuncios />
                <CajaAnuncios />
                <CajaAnuncios />
            </View>
        </ScrollView>

    )


}