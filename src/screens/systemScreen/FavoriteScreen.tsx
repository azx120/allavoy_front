import React, { useContext, useEffect } from 'react'
import { Text, View, TouchableOpacity, StatusBar, Alert, BackHandler } from 'react-native'
import { loginStyles } from '../../styles/styles'
import { AuthContex } from '../../context/UsuarioContext'
import ToolBar from '../../components/Toolbar'
import ElementListNotificacion from '../../components/ElementListNotificacion'
import {color} from '../../styles/colors'
import { useNavigation } from '@react-navigation/native'


export default function FavoriteScreen(props) {
    const navigator = useNavigation()
    const states = [
        {
            id: 1,
            name: 'Notificacion 1',
            date: "15:03  20-12-2021",
            type: 2,
            view: 1,
            phone: '',
            address: ''
        },
       {
            id: 2,
            name: 'Notificacion 2',
            date: "16:28  03-05-2021",
            type: 0,
            view: 0,
            phone: '',
            address: ''
       },
       {
        id: 3,
        name: 'Notificacion 3',
        date: "08:50  18-10-2021",
        type: 1,
        view: 1,
        phone: '',
        address: ''
   }  
    ]
    //const [login, loginAction] = useContext(UserContext)
    return (
        <View style={{ flex: 1}}>
                    <StatusBar
                backgroundColor={color.WHITE}
                barStyle='dark-content'
                translucent={true}
            />
                    <ToolBar titulo='Favoritos'
                onPressLeft={() => navigator.goBack()}
                iconLeft={require('../../sources/img/back.png')}
            />
             <View style={{ alignItems: 'center'}}>
    
  
          <ElementListNotificacion 
                vecinos = {states}
                colorIcon= {color.YELLOW1}
            />
            
           
            
        </View>
        </View>
       

    )
}