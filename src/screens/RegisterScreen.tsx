import React, { useState, useContext } from 'react'
import {
    Text,
    View,
    TouchableOpacity,
    StatusBar,
    ScrollView
} from 'react-native'
import { mainStyles, registroStyles, Registro } from '../styles/styles'
import MyTextInput from '../components/MyTextInput'
import ToolBar from '../components/Toolbar'
import { color } from '../styles/colors'
import { AuthContex } from '../context/UsuarioContext'
import RNPickerSelect from 'react-native-picker-select';
import { CheckBox, SocialIcon, Button } from 'react-native-elements'
import { Formik } from 'formik';
import BotonNumero from '../components/BotonNumero'
import SelectPaisNombre from '../components/SelectPaisNombre'


export default function RegisterScreen(props) {

    const { singUp, user } = useContext(AuthContex)


    return (
        <View style={{ flex: 1 }}>
            <StatusBar backgroundColor={color.WHITE} translucent={true} />


            <View style={[Registro.container, { padding: 10 }]}>
                <View>
                    <Text style={{
                        color: color.SECONDARYCOLOR,
                        fontWeight: "bold",
                        fontSize: 22,
                        textAlign: "center"
                    }}> Regístrese</Text>
                </View>
                <View style={{
                    paddingLeft: 30, paddingRight: 30
                }}>
                    <Formik
                        validateOnMount={true}
                        //validationSchema={loginValidationSchema}
                        initialValues={{ name: '', email: '', sexo: '', country: '', rol: 'user', id: user['id'] }}
                        onSubmit={values => singUp(values)} >

                        {({
                            handleChange,
                            handleBlur,
                            handleSubmit,
                            values,
                            errors,
                            touched,
                            isValid,
                        }) => (
                            <>
                                <MyTextInput placeholder='Nombre' image='user'
                                    onChangeText={handleChange('name')}
                                    onBlur={handleBlur('name')}
                                    value={values.name}

                                />

                                <RNPickerSelect

                                    placeholder={{
                                        label: 'selecione su sexo',
                                        value: '',
                                        color: color.SECONDARYCOLOR,
                                    }}
                                    onValueChange={handleChange('sexo')}
                                    items={[
                                        { label: 'hombre', value: 'hombre', },
                                        { label: 'mujer', value: 'mujer' },
                                    ]}
                                    value={values.sexo}

                                />



                                <MyTextInput keyboardType='email-address' placeholder='E-mail'
                                    image='mail'
                                    onChangeText={handleChange('email')}
                                    onBlur={handleBlur('email')}
                                    value={values.email}
                                />


                                <Text style={{ textAlign: "center", marginTop: 15 }}>Ciudad</Text>

                                <SelectPaisNombre
                                    onValueChange={handleChange('country')}
                                    onBlur={handleBlur('country')}
                                    value={values.country}
                                />

                                <TouchableOpacity
                                    style={{ marginTop: 20,alignItems: "center" }}
                                    onPress={() => handleSubmit()}>
                                    <BotonNumero
                                        TituloNumero='Aceptar y Continuar'
                                        
                                    />
                                </TouchableOpacity>
                            </>
                        )}
                    </Formik>
                </View>
                <View style={{
                    alignItems: "center"
                }}>

                </View>

            </View>
        </View>
    )

}